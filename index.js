const { createNamespace } = require('node-request-context');
const uuidv4 = require('uuid/v4');

class AWSContext {
    constructor(params) {
        const input = params || {};
        this.serviceName = input.serviceName || 'unknown-service';
        this.namespace = input.namespace || 'aws-context';
        this.extract = input.extract;
        this.useAsyncHooks = (typeof input.useAsyncHooks === 'undefined') ? true : input.useAsyncHooks;
        if (this.useAsyncHooks) {
            this.storage = createNamespace(this.namespace);
        }
    }
    async createContext(event, func) {
        if (this.useAsyncHooks) {
            return new Promise((resolve, reject) => {
                this.storage.run(async () => {
                    try {
                        const res = await this.processEvent(event, func);
                        return resolve(res);
                    } catch (err) {
                        return reject(err);
                    }
                });
            });
        } else {
            return await this.processEvent(event, func);
        }
    }
    async processEvent(event, func) {
        this.setContext({});
        this.setVariable('service', this.serviceName);
        this.extractContext(event);
        return func(event);
    }
    extractContext(event) {
        const source = this.getEventSource(event);
        switch (source) {
            case 'plain':
                this.extractContextPlain(event);
                break;
            case 'sqs':
                this.extractMessageAttributes(event);
                break;
            case 'sns':
                this.extractMessageAttributes(event.Sns);
                break;
            case 'api-gateway':
                this.extractContextPlain(event.headers);
                break;
        }
        this.extractCustomVariables(event);
        const corIdKey = `${this.namespace}-correlation-id`;
        if (!this.get(corIdKey)) {
            this.set(corIdKey, `${this.serviceName}-${uuidv4()}`);
        }
    }
    extractContextPlain(event) {
        for (let key of Object.keys(event)) {
            if (key.startsWith(`${this.namespace}-`) && event[key] && !this.get(key)) {
                this.set(key, event[key]);
            }
        }
    }
    extractMessageAttributes(msg) {
        const attrs = msg.messageAttributes || msg.MessageAttributes;
        for (let key of Object.keys(attrs)) {
            if (key.startsWith(`${this.namespace}-`) && attrs[key] && !this.get(key)) {
                const value = attrs[key].stringValue || attrs[key].Value
                this.set(key, value);
            }
        }
    }
    extractCustomVariables(event) {
        for (let varPath of this.extract) {
            const parsedVar = this.getNestedValue(event, varPath);
            const newKey = `${this.namespace}-${varPath}`;
            if (parsedVar && !this.get(newKey)) {
                this.set(newKey, parsedVar);
            }
        }
    }
    insertIntoLambdaParams(params) {
        let copy = Object.assign({}, params);
        let payload = copy.Payload;
        if (payload) {
            try {
                payload = JSON.parse(payload);
            } catch (error) {
                throw new Error('Only stringified JSON payload currently supported.');
            }
        }
        payload = Object.assign(payload || {}, this.getContext());
        copy.Payload = JSON.stringify(payload);
        return copy;
    }
    insertIntoMsgParams(params) {
        if (params.Entries) {
            const newEntries = [];
            for (let entry of params.Entries) {
                newEntries.push(this.insertIntoMessage(entry));
            }
            params.Entries = newEntries;
            return params;
        } else {
            return this.insertIntoMessage(params);
        }
    }
    insertIntoMessage(msg) {
        if (!msg.MessageAttributes) {
            msg.MessageAttributes = {};
        }
        for (let key of Object.keys(this.getContext())) {
            msg.MessageAttributes[key] = {
                DataType: 'String',
                StringValue: this.get(key)
            }
        }
        return msg;
    }
    getEventSource(event) {
        if (event.Records) {
            throw new Error('This event contain Records. Use individual records to preserve contexts.');
        }
        if (event.eventSource === 'aws:sns' || event.EventSource === 'aws:sns') {
            return 'sns';
        }
        if (event.eventSource === 'aws:sqs' || event.EventSource === 'aws:sqs') {
            return 'sqs';
        }
        if (event.path && event.headers) {
            return 'api-gateway'
        }
        return 'plain'
    }
    set(key, value) {
        if (typeof value != 'string'){
            throw new Error('Only string content supported.');
        }
        const ctx = this.getContext();
        ctx[key] = value;
        this.setContext(ctx);
    }
    get(key) {
        return this.getContext()[key];
    }
    getContext() {
        if (this.useAsyncHooks) {
            try {
                return this.storage.get(this.namespace);
            } catch (err) {
                if (err instanceof TypeError) {
                    return undefined;
                }
                throw err;
            }
        } else {
            return global[this.namespace];
        }
    }
    setContext(ctx) {
        if (this.useAsyncHooks) {
            this.storage.set(this.namespace, ctx);
        } else {
            global[this.namespace] = ctx;
        }
    }
    setVariable(key, val) {
        this.set(`${this.namespace}-${key}`, val);
    }
    getVariable(key) {
        return this.get(`${this.namespace}-${key}`);
    }
    getNestedValue(obj, key) {
        return key.split(".").reduce(function (result, key) {
            return result ? result[key] : undefined;
        }, obj);
    }
}
class Proxy {
    constructor(params) {
        this.tracer = new AWSContext(params);
    }
    createContext(event, func) {
        return this.tracer.createContext(event, func);
    }
    insertIntoLambdaParams(params) {
        return this.tracer.insertIntoLambdaParams(params);
    }
    insertIntoMsgParams(params) {
        return this.tracer.insertIntoMsgParams(params);
    }
    getRawContext() {
        return this.tracer.getContext();
    }
    set(key, val) {
        return this.tracer.setVariable(key, val);
    }
    get(key) {
        return this.tracer.getVariable(key);
    }
}
module.exports = Proxy