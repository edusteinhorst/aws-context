const expect = require('chai').expect;
const apiGatewayEvent = require('./sample-api-gateway-event.json');
const sqsEvent = require('./sample-sqs-event.json');
const snsEvent = require('./sample-sns-event.json');
const plainEvent = require('./sample-plain-event.json');
const AWSContext = require('../index');

const options = {
    useAsyncHooks: true,
    serviceName: 'TestService',
    namespace: 'aws-tracer',
    extract: [
        'requestContext.identity.cognitoIdentityId',
        'requestContext.identity.sourceIp',
        'X-Amzn-Trace-Id'
    ]
}
const awsCtx = new AWSContext(options);

async function delay() {
    let sum = 0;
    for (let i = 0; i < 100000000; i++) {
        sum++;
    }
    return sum;
}

describe('Test lambda event tracing', function () {
    it('should wait async function', async function () {
        let sum = await awsCtx.createContext(apiGatewayEvent, async () => {
            return await delay();
        });
        expect(sum).equals(100000000);
    });
    it('should pass event to function correctly', async function () {
        const testFunction = async (event) => {
            return event;
        };
        let result = await awsCtx.createContext(apiGatewayEvent, testFunction);
        expect(result).equals(apiGatewayEvent);
    });
    it('should not throw an exception if not initialized', async () => {
        const myOptions = Object.assign({}, options);
        myOptions.namespace = 'new';
        const testTracer = new AWSContext(myOptions);
        expect(testTracer.getRawContext()).to.be.undefined;
    });
    it('should separate contexts when using async_hooks', async function () {
        await awsCtx.createContext(apiGatewayEvent, async () => {
            awsCtx.set('one', 'one');
            await awsCtx.createContext(apiGatewayEvent, async () => {
                expect(awsCtx.get('one')).undefined;
                awsCtx.set('two', 'two');
            });
            expect(awsCtx.get('two')).undefined;
        });
    });
    it('should mix contexts when using global :(', async function () {
        const myOptions = Object.assign({}, options);
        myOptions.useAsyncHooks = false;
        let tracer = new AWSContext(myOptions);
        await tracer.createContext(apiGatewayEvent, async () => {
            tracer.set('one', 'one');
            await tracer.createContext(apiGatewayEvent, async () => {
                expect(tracer.get('one')).undefined;
                tracer.set('two', 'two');
            });
            expect(tracer.get('two')).exist;
        });
    });
    it('should fetch and store correctly in context - apiGatewayEvent', async function () {
        await awsCtx.createContext(apiGatewayEvent, (event) => {
            awsCtx.set('myKey', 'myValue');
            const ctx = awsCtx.getRawContext();
            expect(ctx['aws-tracer-requestContext.identity.sourceIp']).equals(apiGatewayEvent.requestContext.identity.sourceIp);
            expect(ctx['aws-tracer-requestContext.identity.cognitoIdentityId']).equals(apiGatewayEvent.requestContext.identity.cognitoIdentityId);
            expect(ctx['aws-tracer-myKey']).equals('myValue');
        });
    });
    it('should fetch and store correctly in context - sqsEvent', async function () {
        for (let event of sqsEvent.Records) {
            await awsCtx.createContext(event, (event) => {
                awsCtx.set('myKey', 'myValue3');
                const ctx = awsCtx.getRawContext();
                // console.log("ctx->\n", ctx);
                expect(ctx['aws-tracer-sample']).equals("MySample");
                expect(ctx['aws-tracer-myKey']).equals('myValue3');
                expect(awsCtx.get('myKey')).equals('myValue3');
            });
        }
    });
    it('should fetch and store correctly in context - snsEvent', async function () {
        for (let event of snsEvent.Records) {
            await awsCtx.createContext(event, (event) => {
                awsCtx.set('myKey', 'myValue3');
                const ctx = awsCtx.getRawContext();
                // console.log("ctx->\n", ctx);
                expect(ctx['aws-tracer-sample']).equals("MySample");
                expect(ctx['aws-tracer-myKey']).equals('myValue3');
                expect(awsCtx.get('myKey')).equals('myValue3');
            });
        }
    });
    it('should block message batch - sqsEvent', async function () {
        let error;
        try {
            await awsCtx.createContext(sqsEvent, async (event) => {
            });
        } catch (err) {
            error = err;
        }
        expect(error).to.exist;
        expect(error.message).contain('Use individual records');
    });
    it('should block non string context var', async function () {
        let error;
        try {
            await awsCtx.createContext(apiGatewayEvent, async (event) => {
                awsCtx.set('fail', { tapOn: false });
            });
        } catch (err) {
            error = err;
        }
        expect(error).to.exist;
        expect(error.message).contain('Only string content supported.');
    });
    it('should fetch and store correctly in context - plainEvent', async function () {
        await awsCtx.createContext(plainEvent, (event) => {
            awsCtx.set('myKey', 'myValue2');
            const ctx = awsCtx.getRawContext();
            // console.log("ctx->\n", ctx);
            expect(ctx['aws-tracer-sample']).equals("MySample2");
            expect(ctx['aws-tracer-myKey']).equals('myValue2');
            expect(awsCtx.get('myKey')).equals('myValue2');
        });
    });
    it('should insert into sqs message', async function () {
        await awsCtx.createContext(sqsEvent.Records[0], (event) => {
            let params = {
                MessageAttributes: {
                    type: {
                        DataType: 'String',
                        StringValue: 'DepositRequestCreated',
                    },
                },
                MessageBody: JSON.stringify({ id: 244 }),
                QueueUrl: 'dada',
            };
            const modded = awsCtx.insertIntoMsgParams(params);
            // console.log(modded);
            expect(modded.MessageAttributes['aws-tracer-sample'].StringValue).equals('MySample');
        });
    });
    it('should insert into sqs message empty attributes', async function () {
        await awsCtx.createContext(sqsEvent.Records[0], (event) => {
            let params = {
                MessageBody: JSON.stringify({ id: 244 }),
                QueueUrl: 'dada',
            };
            const modded = awsCtx.insertIntoMsgParams(params);
            // console.log(modded);
            expect(modded.MessageAttributes['aws-tracer-sample'].StringValue).equals('MySample');
        });
    });
    it('should insert into sqs batch', async function () {
        await awsCtx.createContext(sqsEvent.Records[0], (event) => {
            let params = {
                MessageAttributes: {
                    type: {
                        DataType: 'String',
                        StringValue: 'DepositRequestCreated',
                    },
                },
                MessageBody: JSON.stringify({ id: 244 }),
                QueueUrl: 'dada',
            };
            const batch = {
                Entries: [params]
            }
            const modded = awsCtx.insertIntoMsgParams(batch);
            // console.log(JSON.stringify(modded));
            expect(modded.Entries[0].MessageAttributes['aws-tracer-sample'].StringValue).equals('MySample');
        });
    });
    it('should insert into lambda params', async function () {
        await awsCtx.createContext(sqsEvent.Records[0], (event) => {
            let body = {
                scriptId: "customer-withdraw-begin",
            }
            let event2 = {
                pathParameters: {
                    id: 123,
                    deep: {
                        deeper: 1
                    }
                },
                body: JSON.stringify(body)
            };
            const params = {
                FunctionName: 'adada',
                Payload: JSON.stringify(event2)
            };

            const modded = awsCtx.insertIntoLambdaParams(params);
            expect(JSON.parse(params.Payload)['aws-tracer-sample']).undefined;
            expect(JSON.parse(modded.Payload)['aws-tracer-sample']).equals('MySample');
            expect(JSON.parse(modded.Payload).pathParameters.deep.deeper).equals(1);
        });
    });
    it('should insert into lambda params without payload', async function () {
        await awsCtx.createContext(sqsEvent.Records[0], (event) => {
            const params = {
                FunctionName: 'adada',
            };
            const modded = awsCtx.insertIntoLambdaParams(params);
            // console.log(modded);
            expect(JSON.parse(modded.Payload)['aws-tracer-sample']).equals('MySample');
        });
    });
    it('should refuse lambda string payload', async function () {
        await awsCtx.createContext(sqsEvent.Records[0], (event) => {
            const params = {
                FunctionName: 'adada',
                Payload: 'gugugugu'
            };
            let error;
            try {
                awsCtx.insertIntoLambdaParams(params);
            } catch (err) {
                error = err;
            }
            expect(error).to.exist;
            expect(error.message).contain('Only stringified JSON payload currently supported.');
        });
    });
});