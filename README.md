# AWS Context
[![pipeline status](https://gitlab.com/edusteinhorst/aws-context/badges/master/pipeline.svg)](https://gitlab.com/edusteinhorst/aws-context/commits/master)
[![coverage report](https://gitlab.com/edusteinhorst/aws-context/badges/master/coverage.svg)](https://gitlab.com/edusteinhorst/aws-context/commits/master)

## Overview

This module extracts context information from AWS sources, and passes it on to other AWS services. It can be used for passing variables across microservices, such as correlation-id, user identity, source IP, etc. **without having to pass them manually through the call stack**. Useful for logging tracing information, for example. 

It will automatically generate an UUIDv4 correlation id based on your service name, if none is present on the event. Otherwise it will detect and pass it forward. All variables (including correlation id) are segregated by the namespace (see options).

There are two possible ways to preserve state througout the call stack:

### Using async_hooks

This uses the new Node async_hooks (Node v8+) for Continuation Local Storage, which acts like thread-local storage for Node. Be aware that async_hooks is as of now an *experimental feature* in Node. This feature allows contexts to be bound to a specific call stack, even across async calls, allowing the storage to be segregated between client requests. However, async_hooks currently have a [**an impact on performance**](https://github.com/nodejs/benchmarking/issues/181).

### Using a global variable

If you are using Lambdas, is is possible to use a global variable as an alternative, which doesn't add any significant performance impact. This is due to AWS processing events sequentially per Lambda instance, with no concurrency. The caveat here is if your Lambda processes a batch event source (e.g. SQS or SNS batch). In that case each message in the batch could contain a different context (i.e. a different user request), which will not present any problems, **if you don't process them in parallel**. If you need parallel processing, use async_hooks.

## Usage

### Configuration

Besides defining a service name and a namespace for content variables, it is possible to extract data from objects deep in the event using the dot notation. For example, use 'requestContext.identity.cognitoIdentityId' to extract the Cognito user id from an API Gateway event. 

Load the code below as a module, so that that configuration is done only once:

    const options = {
        useAsyncHooks: true,
        serviceName: process.env.SERVICE_NAME,
        namespace: 'aws-context',
        extract: [
            'requestContext.identity.cognitoIdentityId',
            'requestContext.identity.sourceIp',
            'X-Amzn-Trace-Id'
        ]
    }
    const ctx = new AWSContext(options);

    module.exports = ctx;

### Context extraction/creation

Event source is automatically identified (e.g. API Gateway, Lambda, SQS, SNS). Use the following pattern as your async event handler:

    handler: async (event) => {
        return ctx.createContext(event, () => {
            ... your logic goes here
        });
    }

This will do two things:

1. Extract context information from the event. 
2. Invoke your function inside an isolated context (if using async_hooks).

### Context passing across services

For context data to be preserved across services, wrap your AWS calls like such:

    const res1 = await lambda.invoke(ctx.insertIntoLambdaParams(params)).promise();
    const res2 = await sqs.sendMessage(ctx.insertIntoMsgParams(params)).promise();
    const res3 = await sns.publish(ctx.insertIntoMsgParams(params)).promise();

### Accessing context data

Context data can be accessed and modified:

    // get raw context - useful for logging, for example
    const rawCtx = ctx.getRawContext();

    // add more info to context
    ctx.set('customerId', 1234);
    
    // retrieve info from context
    const customerId = ctx.get(key);

**Credits**

icon by Nithinan Tatah from the Noun Project